package epsi.b3c3.tpgrp;

import junit.framework.TestCase;

public class AdditionTest extends TestCase {

	private Addition monAdd = new Addition();

	/**
	 * Rigorous Test :-)
	 */
	public void testAddPositifs() {

		monAdd.setOperande1(12);
		monAdd.setOperande2(10);

		int resultat = monAdd.addEntiers();
		assertEquals(22, resultat);
	}

	public void testAddNegatifs() {

		monAdd.setOperande1(-12);
		monAdd.setOperande2(10);

		int resultat = monAdd.addEntiers();
		assertEquals(-2, resultat);
	}

}
