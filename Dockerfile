FROM java:latest
# WORKDIR /
ADD target/epsi.b3c3.tpgrp-0.0.1-SNAPSHOT.jar target/epsi.b3c3.tpgrp-0.0.1-SNAPSHOT.jar
# EXPOSE 8080
# Il y a 2 manières de faire : soit démarrer notre app depuis le Dockerfile,
# soit on execute la commande dans le docker-compose, ce qui est plus propre (pk?)
CMD java -jar target/epsi.b3c3.tpgrp-0.0.1-SNAPSHOT.jar
